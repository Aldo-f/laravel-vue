# Frontend

## Create new view

* Create a new view named ``Laravel.vue``
* Add the view to the router links on ``App.vue``
* And add the view to ``router.js``

## Show data from database
(SPA = Single page application)

1. Set up database <br>
   ``php artisan make:migration CreateCrudsTable``   
   *database/migrations/YYY_MM_DD_create_cruds_table.php*
   ``` php
   public function up()
   {
    Schema::create('cruds', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->text('name');
      $table->text('color');
      $table->timestamps();
    });
   }
   ```
   and migrate:
   ``` sh
   lando ssh 
   php artisan migrate && exit
   ```

2. API <br>
   *routes/api.php*
   ``` php
   <?php

    Route::resource('/cruds', 'CrudsController', [
    'except' => ['edit', 'show', 'store']
    ]);
   ```
3. Controller <br>
   ``php artisan make:controller CrudsController && php artisan make:model Crud``
   ``` php
    class CrudsController extends Controller
    {
        public function index()
        {
            return response(Crud::all()->jsonSerialize(), Response::HTTP_OK);
        }

        public function create(Generator $faker)
        {
            $crud = new Crud();
            $crud->name = $faker->lexify('????????');
            $crud->color = $faker->boolean ? 'red' : 'green';
            $crud->save();

            return response($crud->jsonSerialize(), Response::HTTP_CREATED);
        }

        public function update(Request $request, $id)
        {
            $crud = Crud::findOrFail($id);
            $crud->color = $request->color;
            $crud->save();

            return response(null, Response::HTTP_OK);
        }

        public function destroy($id)
        {
            Crud::destroy($id);

            return response(null, Response::HTTP_OK);
        }
    }
   ```


### Useful terminal commands
```
vue ui
vue add bootstrap-vue
```


Sources:
* https://vuejs.org/v2/guide/
* https://vuejs.org/v2/guide/components.html 
* https://laravel-news.com/using-vue-router-laravel
* https://vuejsdevelopers.com/2018/02/05/vue-laravel-crud/
* https://github.com/anthonygore/vue-laravel-crud/
* https://bootstrap-vue.js.org/docs/#vue-cli-3-plugin
