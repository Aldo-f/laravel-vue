import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import App from './App'
import Vue from 'vue'
import VueAxios from './plugins/axios'
import router from './router'

Vue.config.productionTip = false

new Vue({
    router,
    VueAxios,
    render: h => h(App),
}).$mount('#app')
